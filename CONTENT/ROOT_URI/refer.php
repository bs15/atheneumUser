<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>
<script type="text/javascript">
  function sendMail(email, subject, message) {
    let formData = new FormData();
      formData.append('sendMail', 'true');
      formData.append('reciever', email);
      formData.append('sender', 'Admin');
      formData.append('senderMail', 'no-reply@atheneumglobal.com');
      formData.append('subject', subject);
      formData.append('message', message);
      fetch("https://mailapi.teenybeans.in/", {
          method: "POST",
          body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
  }
</script>
<?php 
 // function httpPost($url,$params)
 //  {
 //    $postData = '';
 //     //create name value pairs seperated by &
 //   foreach($params as $k => $v) 
 //   { 
 //      $postData .= $k . '='.$v.'&'; 
 //   }
 //   $postData = rtrim($postData, '&');
 
 //    $ch = curl_init();  
 
 //    curl_setopt($ch,CURLOPT_URL,$url);
 //    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 //    curl_setopt($ch,CURLOPT_HEADER, false); 
 //    curl_setopt($ch, CURLOPT_POST, count($postData));
 //        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
 
 //    $output=curl_exec($ch);
 
 //    curl_close($ch);
 //    return $output;
   
 //  }

  $link = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $email = $_SESSION["user"];
  $userId = $_SESSION["userId"];
  // --------- CURRENT USER DETAILS ----------------
  $sqlUserDetails = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$email'";
  $resultUserDetails = mysqli_query($link,$sqlUserDetails);
  $rowUserDetails = mysqli_fetch_array($resultUserDetails,MYSQLI_ASSOC);
  $earning = $rowUserDetails['EARNING'];
  $jsonDetails = $rowUserDetails["DETAILS"];
  $jsonDetails = json_decode($jsonDetails);
  if ($jsonDetails != null) {
    $country = $jsonDetails->{'country'};
    $state = $jsonDetails->{'state'};
    if ($country == "India") {
     $in = true;
    }else{
      $in = false;
    }
  }


  //-------------- SUBMIT EMAIL REFERENCE -----------
  if (isset($_POST['submit'])) {
    $reference = $_POST['referEmail'];
    //check if the refence already present or not
    $count = 0;
    $sqlCheck = "SELECT * FROM ATHENEUM_STUDENT";
    $resultCheck = mysqli_query($link,$sqlCheck);
    if(mysqli_num_rows($resultCheck)>0){
      while($rowCheck = mysqli_fetch_array($resultCheck,MYSQLI_ASSOC)){
        if (in_array($rowCheck['EMAIL'], $reference)) {
          $count++;
        }
      }
    }
    if ($count>0) {
     echo '<div class="container"><div class="alert alert-warning">That email is already registered.</div></div>'; 
    }else{
      $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$email'";
      $result = mysqli_query($link,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $referals = $row['REFERALS'];
      $userName = $row['NAME'];
      if ($referals == null) {
        $referals = array_filter($reference);
        $referals = array_unique($referals);
        $referals = implode(",", $referals);
      }else{
        $referals = explode (",", $referals);
        $referals = array_merge($referals, $reference);
        $referals = array_filter($referals);
        $referals = array_unique($referals);
        $referals = implode(",", $referals);
      }
      $sql = "UPDATE ATHENEUM_STUDENT SET REFERALS = '$referals' WHERE EMAIL='$email'";
      $result = mysqli_query($link, $sql);
      if($result){
        // // $reference = implode(",", $reference);
        // $params = array(
        //  "sendMail" => true,
        //  "reciever" => $reference,
        //  "sender" => "Atheneum Global",
        //  "senderMail" => "noreply@atheneumglobal.com",
        //  "subject" => "Referal From ".$userName,
        //  "message" => "<html>
        //                 <head>
        //                 <title>Welcome To Athemeum Global College</title>
        //                 </head>
        //                 <body>
        //                 <h2>Welcome to Atheneum Global Teacher Traing College </h2>
        //                 <h3>". $userName ." has sent you a referal of Atheneum Global Course. Hurry up and take admission to get attractive offers.Sign up today to avail of an exciting Rewards Program from Atheneum.</h3>
        //                 <h3><a href='https://atheneumglobal.education/enroll'>Click here to see our courses.</a></h3>
        //                 <h3>You can also sign up for for the referal Program <a href='https://user.athenemglobal.education'>here</a>.</h3>
        //                 <h4>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h4>
        //                 </body>
        //                 </html>"
        // );
        // $response = httpPost("https://api.teenybeans.in/API/mailApi/v1/",$params);
            // $to = $email;
            $subject = "New Referral From ".$userName;
            $message = "
            <html>
                <head>
                <title>Welcome To Athemeum Global College</title>
                </head>
                <body>
                <h2>Welcome to Atheneum Global Teacher Traing College </h2>
                <h3>". $userName ." has sent you a referal of Atheneum Global Course. Hurry up and take admission to get attractive offers.Sign up today to avail of an exciting Rewards Program from Atheneum.</h3>
                <h3><a href='https://atheneumglobal.education/enroll'>Click here to see our courses.</a></h3>
                <h3>You can also sign up for for the referal Program <a href='https://user.athenemglobal.education'>here</a>.</h3>
                <h4>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h4>
                </body>
                </html>
            ";
            foreach ($reference as $key => $value) {
              echo '<script>sendMail(`'.$value.'`, `'.$subject.'`, `'.$message.'`)</script>';
            }
             
        // echo $response;
        echo "<script>location.reload()</script>";
      }
    }
  }
 
 ?>

 <?php if (!$_SESSION['LoggedIn']){
  header("Location: signIn");
 }


 ?>

<?php if($_SESSION['LoggedIn']): ?>
<div class="content-wrapper">
  <section class="content">
    <br>
<div class="container">
  
   <div class="row">
        <div class="col-md-12">
          <h1 class="display-7 text-center">Refer & Earn Program</h1>
        
          <!-- Dashboard Actions -->

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-md-10 col-lg-10 col-sm-12 ml-auto mr-auto">
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                  <div class="card">
              
              <div class="card-header">
                <h3 class="card-title">Give references</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                <form method="POST">
                  <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="input-group mb-3">
                        <input type="email" name="referEmail[]" placeholder="Give an Email" class="form-control" required>
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="input-group mb-3">
                        <input type="email" name="referEmail[]" placeholder="Give an Email" class="form-control">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="input-group mb-3">
                        <input type="email" name="referEmail[]" placeholder="Give an Email" class="form-control">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-12 ml-auto mr-auto">
                    <input type="submit" name="submit" class="btn btn-success btn-block" value="Submit Reference">
                    </div>
                </form><div id="fb-root"></div>
                <!-- <button style="font-size:24px">Button <i class="fa fa-whatsapp"></i></button> -->
              </div> 
              <div class="card-header">
                  <h3 class="card-title">Share <i class="fas fa-share"></i></h3>
              </div>
              <div class="card-body">
               <div class="btn btn-primary" data-href="https://atheneumglobal.education/" data-layout="button_count" data-size="large"><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fatheneumglobal.education%2F&amp;src=sdkpreparse&quote=Hey%2C%20I%20have%20signed%20up%20for%20Atheneum%20Global%20to%20pursue%20an%20International%20Teaching%20Diploma.%20And%20I%27ve%20got%20exciting%20rewards%20too%21%20Sign%20up%20today%20to%20avail%20of%20an%20exciting%20Rewards%20Program%20from%20Atheneum." class="fb-xfbml-parse-ignore"><i class="fa fa-facebook" style="font-size:20px; color: #fff;"></i></a></div>
                <!-- <a href="https://wa.me/?text=I%20found%20a%20great%20website.%20Check%20out%20this%20link%20https%3A%2F%2Fwww.example.com%2F">Share on WhatsApp</a> -->
                <a href="whatsapp://send?text=Hey, I have signed up for Atheneum Global to pursue an International Teaching Diploma. And I've got exciting rewards too! Sign up today to avail of an exciting Rewards Program from Atheneum. Signup using below link to get the exciting Rewards. Go to-> https://user.atheneumglobal.com/signUp?refer=<?php echo $userId; ?>" data-action="share/whatsapp/share"><button class="btn btn-success"><i class="fa fa-whatsapp" style="font-size:20px;"></i></button></a>
                <a href="sms:?body=Hey, I have signed up for Atheneum Global to pursue an International Teaching Diploma. And I've got exciting rewards too! Sign up today to avail of an exciting Rewards Program from Atheneum.Signup using below link to get the exciting Rewards. Go to-> https://user.atheneumglobal.com/signUp?refer=<?php echo $userId; ?>" class="btn btn-warning"><i class="fas fa-envelope" style="font-size:20px; color: #fff;"></i></a>
              </div>

              <!-- /.card-body -->
            </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                  <video width="100%" height="auto" controls style="border: 1px solid black;" >
                  <source src="/VIDEOS/refer.mp4" type="video/mp4">
                </video>
                </div>
              </div>
            
            <!-- /.card -->
            <div class="card card-success card-tabs">
      <div class="card-header p-0 pt-1">
        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">My Referrals</a>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">
          <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
              <p class="float-right"><b>Total Earning:- <?php if($in) echo "Rs. ".$earning; else echo "$".$earning; ?></b></p>
              <div class=" table-responsive p-0">
                <table class="table table-hover text-nowrap" id="franchiseList">
                  <thead>
                    <tr>
                      <th scope="col">Serial No</th>
                      <th scope="col">Email ID's</th>
                      <th scope="col">Registered</th>
                      <th scope="col">Enrolled</th>
                      <th scope="col">My Earning</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php 
                      $referals = $rowUserDetails['REFERALS'];
                      if ($referals == null) {
                        echo '<div class="alert alert-warning">No Refarals yet! Start referring now!</div>'; 
                      }else{
                        $referals = explode (",", $referals);
                      $sql = "SELECT * FROM ATHENEUM_STUDENT";
                      $result = mysqli_query($link,$sql);
                      $registeredUser = array();
                      $enrolledUser = array();
                      if(mysqli_num_rows($result)>0){
                        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                          $userEmail = $row['EMAIL'];
                          $paid = $row['PAID'];
                          if($userEmail != $email){
                            if (in_array($userEmail, $referals)){
                              array_push($registeredUser, $userEmail);
                            }
                            if ($paid != 0) {
                              array_push($enrolledUser, $userEmail);
                            }
                          }
                          
                        }
                      }
                      if ($referals == null) {
                        echo '<div class="alert alert-warning">No referrals yet! </div>';
                      }else{
                        
                        foreach ($referals as $key => $value) { ?>
                          <tr>
                            <td scope="col"><?php echo $key+1 ?></td>
                            <td scope="col"><?php echo $value ?></td>
                            <td scope="col"><?php if(in_array($value, $registeredUser)) echo '<i class="fas fa-check-circle"></i>';else echo '<i class="fa fa-times-circle-o" aria-hidden="true"></i>' ?></td>
                            <td scope="col"><?php if(in_array($value, $enrolledUser)) echo '<i class="fas fa-check-circle"></i>';else echo '<i class="fa fa-times-circle-o" aria-hidden="true"></i>' ?></td>
                            <?php 
                              $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$value'";
                              $result = mysqli_query($link,$sql);
                              $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
                              if ($in) {
                                if ($row['PROGRAM_TYPE'] == "Certification Course") {
                                $earnedMoney = "Rs. 500";
                                }else if ($row['PROGRAM_TYPE'] == "Graduate Course") {
                                  $earnedMoney = "Rs. 750" ;
                                }else if ($row['PROGRAM_TYPE'] == "Post Graduate Course") {
                                  $earnedMoney = "Rs. 1000";
                                }else{
                                  $earnedMoney = 0;
                                }
                              }else{
                                if ($row['PROGRAM_TYPE'] == "Certification Course") {
                                $earnedMoney = "$10";
                                }else if ($row['PROGRAM_TYPE'] == "Graduate Course") {
                                  $earnedMoney = "$15" ;
                                }else if ($row['PROGRAM_TYPE'] == "Post Graduate Course") {
                                  $earnedMoney = "$20";
                                }else{
                                  $earnedMoney = 0;
                                }
                              }
                              
                             ?>
                            <td scope="col"><?php echo $earnedMoney; ?></td>
                          </tr>
                        <?php }
                        
                      }
                              
                    } ?> 
                            
                    
                  </tbody>
                </table>
              </div>
          </div>
          <!--  -->
        </div>
      </div>
      <!-- /.card -->
    </div>
          </div>
        </div>
          

        </div>
      </div>
</div>
<?php else: ?>
  <h1 class="text-center">Please Signin</h1>

<?php endif; ?>