<?php
$link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
$link->set_charset("utf8");
if (isset($_SESSION["user"])) {
  $email = $_SESSION["user"];
  $userId = $_SESSION["userId"];
}else{
  $email = null;
  $userId = null;
}



if(mysqli_connect_error()){
   die("ERROR: UNABLE TO CONNECT: ".mysqli_connect_error());
}

if (isset($_POST['logout'])) {
    session_destroy();
    echo "<script>
      window.location.href='/';
     </script>";
}

 $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$userId'";

 $result = mysqli_query($link,$sql);

 if($result){
      if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $userName = $row["NAME"];
        $name = explode(" ", $userName);
        $userFirst = $name[0];
      }
  }
 ?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/CSS/DIZ.css" >
    <link rel="stylesheet" href="/CSS/style.css" >
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

     <link rel="stylesheet" href="/CSS/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
    <link rel="stylesheet" href="/CSS/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="/CSS/plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/CSS/dist/css/adminlte.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/CSS/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- <link rel="stylesheet" href="/CSS/video.css" > -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <script src="https://kit.fontawesome.com/e1f2cafe0a.js"></script> -->

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script>
    <script type="text/javascript" src="/JS/vue.min.js"></script>



   <!--  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
       -->

    <title>Atheneum Global College</title>
    <style type="text/css">
      .btn-grad {background-image: linear-gradient(to right, #AA076B 0%, #61045F 51%, #AA076B 100%)}
      .btn-grad:hover { background-position: right center; } 
      body{
        padding-top: 60px;
      } 
    </style>
  </head>
  <body class="hold-transition  layout-top-nav">
    <div class="wrapper">
      <form id="logoutForm" method="POST" id="logoutform">
        <input type="hidden" name="logout" id="logout">
      </form>

  <!-- NAVBAR STARTS -->
  <nav class="main-header fixed-top navbar navbar-expand-md navbar-light navbar-white shadow">
    <div class="container">
      <a href="/" class="navbar-brand">
        <img src="/IMAGES/favicon.ico" alt="" class="brand-image" style="height: 40px; width:auto;">
        <span class="btn" style="font-size: 1.2em; font-weight: bold; color: #9c2e38;">Atheneum</span>
      </a>
      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
      <?php if ($_SESSION['LoggedIn']): ?>
        <ul class="navbar-nav">
          <li class="nav-item" style="margin-right: 5px;">
            <a href="/" class="btn btn-outline-success">Dashboard</a>
          </li>
          <li class="nav-item" style="margin-right: 5px;">
            <a href="refer" class="btn btn-grad animated infinite tada delay-2s slow" style="color: #fff;">Refer & Earn</a>
          </li>
        </ul>
      <?php endif; ?>
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
      <?php if ($_SESSION['LoggedIn']): ?>
        <li class="nav-item">
          <a class="btn btn-warning" href="signOut">
            Sign out
          </a>
        </li>
        
        <!------------- NOTIFICATION LOGIC ----------->
        <?php 
          $sqlNoti = "SELECT * FROM ATHENEUM_NOTIFICATION WHERE USER_ID='$userId'";
          $resultNoti = mysqli_query($link, $sqlNoti);
          $active = 0;
          while($row = mysqli_fetch_array($resultNoti,MYSQLI_ASSOC)){
            if($row['STATUS'] == 'PENDING'){
              $active++;
            }
          }

         ?>
        
        <?php else: ?>
          <li class="nav-item" style="margin-right:5px;">
            <a href="signIn" class="btn" style="font-size: 1.2em; font-weight: bold; color: #9c2e38;">Sign In</a>
          </li>
          <li class="nav-item">
            <a href="signUp" class="btn" style="font-size: 1.2em; font-weight: bold; color: #9c2e38;">Register</a>
          </li>
        <?php endif; ?>

      </ul>
       

      </div>
      <?php if ($_SESSION['LoggedIn']) { ?>
        
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="myProfile">
            <?php echo $userFirst; ?>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="notification">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge"><?php if($active !=0) echo $active; ?></span>
          </a>
        </li>
      </ul>

       <?php } ?>
     

      <!-- Right navbar links -->
      
       
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>
  <!-- /.navbar -->

   
  
 
  

