<style type="text/css">
  .full{
    width: 100%;
    height: 100vh;
    background-image: linear-gradient(to right bottom, #9c2e38, #b3404b, #ca525e, #e16473, #f87788);
    color: #fff;
  }
  .leftside{
    padding-top: 15%;
  }
  .rightside{
    padding-top: 7%;
    text-align: left;
  }
  .btn-round{
    background: #fff;
    color: #9c2e38;
    font-weight: bold;
    border-radius: 17px;
  }
  .heroImg{
    width: auto;
    height: 500px;
  }
  @media only screen and (max-width: 600px) {
    .full{
      width: 100%;
      height: auto;
      background-image: linear-gradient(to right bottom, #9c2e38, #b3404b, #ca525e, #e16473, #f87788);
      color: #fff;
      padding-bottom: 30px;
    }
    .leftside{
      padding-top: 15%;
      text-align: center;
    }
    .heroImg{
      margin-top: 15px;
      height: 300px;
    }
    .rightside{
      padding-top: 7%;
      text-align: center;
    }
    .heading{
      font-size: 1.2em;
    }
  }
</style>
<link rel="stylesheet" type="text/css" href="/JS/aos/aos.css">
<div class="full">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-12 leftside order-2" data-aos="fade-down-right" data-aos-duration="2000">
        <h5 class="font-weight-bold">Atheneum Global Teacher Training College</h5>
        <h1 class="font-weight-bold heading">Free Online Certificate Program in Montessori Education<br>(Demo Course)</h1>
        <a href="#aboutUs" class="btn btn-round mt-3">About Us</a>
        <a href="signIn" class="btn btn-round ml-2 mt-3">Get Started</a>
      </div>
      <div class="col-md-5 col-sm-12 rightside order-1" data-aos="fade-up-left" data-aos-duration="2000">
        <img src="/IMAGES/atheneumcourse.jpeg" class="heroImg">
        <!-- <img src="/IMAGES/online-education.png" class="heroImg"> -->
      </div>
    </div>
  </div>
</div>
<div id="aboutUs" class="content-wrapper pt-2">
  <section class="content">
    <div class="container" data-aos="fade-up" data-aos-duration="1000">
    <div class="col-md-12 col-sm-12 ml-auto mr-auto py-4">
      <div class="register-logo mb-3">
        <h2 class="font-weight-bold" style="color: #9c2e38; text-decoration: underline;">About Us</h2>
      </div>
      <div class="mt-4">
          <div class="row">
            <div class="col-md-6 col-sm-12 text-center">
              <img src="/IMAGES/online-education.png" class="mt-4" height="auto" width="90%">
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="card-text px-3 pt-2" style="font-size: 1.2em;">
                <p>Atheneum Global Teacher Training College offers a Certificate Program in Montessori Teacher Training for those who intend to work in, or be in charge of Montessori house of children anywhere in the world. The course comprises of core Montessori concepts that Montessori Educators must be well acquainted with to become effective Montessorians. The modules of the Certification Program is as under -</p>
                <ol>
                  <li>Life History Of Maria Montessori</li>
                  <li>Montessori method and history</li>
                  <li>Exercises of Practical Life</li>
                  <li>Sensory Education in Montessori</li>
                </ol>
                <p>At the end of the Certificate Program, candidates are awarded an internationally recognized achievement certificate from Atheneum Global Teacher College.  </p>
                <p>Aspirants now have the opportunity to sample an entire module of the Certificate Program. This <a href="signUp">free online Montessori Teacher Training Program </a> is a non-certification module from our Certificate Course in Montessori Education. <a href="signUp">Enroll</a> today to get access. At the end of the module, you would be able to understand how online learning on the Atheneum Platform works and would then be able to take an informed decision subscribe to any of the courses on the Atheneum Platform.</p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="/JS/aos/aos.js"></script>
  <script>
  AOS.init();
</script>