<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<script type="text/javascript"> // jquery ajax call
  $(document).ready(function(){  
    $('#confirmPayment').click(function(){
      var userId = $('#userId').html();
      // console.log(userId);
      $.ajax({
          url: "/API/V1/?confirmPayment",
          data: { user: userId },
          dataType:"html",
          type: "post",
          success: function(data){
             console.log(data);
             location.reload(true);
          }
      });
    });
   });
</script>
<?php 
	$link = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $userId = $_SESSION["userId"];
  $sql = "UPDATE ATHENEUM_NOTIFICATION SET STATUS = 'READ' WHERE USER_ID = '$userId'";
  $result = mysqli_query($link,$sql);

 ?>

 <?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }


 ?>

<?php if($_SESSION['LoggedIn']): ?>
<div class="content-wrapper">
  <section class="content">
    <br>
<div class="container">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7 text-center">Notification</h1>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-6 ml-auto mr-auto">
            <?php 
              $sqlNoti = "SELECT * FROM ATHENEUM_NOTIFICATION WHERE USER_ID='$userId' ORDER BY ID DESC";
              $resultNoti = mysqli_query($link, $sqlNoti);
              while($row = mysqli_fetch_array($resultNoti,MYSQLI_ASSOC)){ 
                  $title = $row['TITLE'];
                  $msg = $row['MESSAGE'];
                  $link = $row['LINK'];
                ?>
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title"><?php echo $title; ?></h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <h4 class="card-text"><?php echo $msg ?></h4>
                    <a class="btn btn-success" href="<?php echo $link; ?>">Click</a>
                  </div> 
                  <!-- /.card-body -->
                </div>
                <!-- /.card --> 
              <?php }
             ?>
          </div>
        </div>
          

        </div>
      </div>
</div>

<!-- MODAL FOR COURSE ACCESS DETAILS -->


<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="register-logo">
        <h2><b>Atheneum Global Teacher Training College</b></h2>
        <p>User's Dashboard portal</p>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="ml-auto mr-auto text-center">
            <img src="/IMAGES/logo.jpeg" width="50%" height="50%">
          </div>
          <div class="card-text">Welcome to Atheneum Global Teacher Training College College User Dashboard. 
            You can win exciting rewards and keep adding cashback into your wallet.  
            <a href="signUp">Register</a> and <a href="signIn">login</a> now to learn more...</div>
         
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </div>

<?php endif; ?>
