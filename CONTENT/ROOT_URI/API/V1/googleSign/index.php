<?php

if(isset($_POST['idtoken'])){
//   $flnk_tvar0="/home/s/web/lib.siliconpin.com/public_html/lib/google-api-php-client-2.2.2/vendor/autoload.php";
  $flnk_tvar0=APP_DIR."/google-api-php-client-2.2.2/vendor/autoload.php";

//   $flnk_tvar0=APP_DIR."/google-auth-library-php-master/autoload.php";
    if(file_exists($flnk_tvar0) )
    {
        require_once $flnk_tvar0;

        // Get $id_token via HTTPS POST.
        $id_token=$_POST['idtoken'];
        $CLIENT_ID=$GLOBALS['google_client_id'];
        $client = new Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
        $payload = $client->verifyIdToken($id_token);
        if ($payload && $payload['email_verified']==true) {
        $userid = $payload['sub'];
        // If request specified a G Suite domain:
        // $domain = $payload['hd'];
        // "email": "testuser@gmail.com",
        // "email_verified": "true",
        // "name" : "Test User",
        // "picture": "https://lh4.googleusercontent.com/-kYgzyAWpZzJ/ABCDEFGHI/AAAJKLMNOP/tIXL9Ir44LE/s99-c/photo.jpg",
        // "given_name": "Test",
        // "family_name": "User",
        // "locale": "en"
        $UserData['loginAttempt']=true;
        $UserData['message']="Login Success!";
        $UserData['sub']=$payload['sub'];//Google User Id
        $UserData['given_name']=$payload['given_name'];//First Name
        $UserData['family_name']=$payload['family_name'];//Last Name
        $UserData['name']=$payload['name'];//Full Name
        $UserData['locale']=$payload['locale'];// Preferred Language
        $UserData['email']=$payload['email'];//Email Id
        $UserData['picture']=$payload['picture'];

        $_SESSION['SIGNEDIN']='yes';
        $_SESSION['sub']=$payload['sub'];
        $_SESSION['given_name']=$payload['given_name'];
        $_SESSION['name']=$payload['name'];
        $_SESSION['locale']=$payload['locale'];
        $_SESSION['picture']=$payload['picture'];
        $_SESSION['email']=$payload['email'];


        // $conn = new mysqli('localhost', 'test_colleger', 'simple2pass', 'test_colleger');
        $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
        if($conn->connect_error) die("Connection failed: " . $conn->connect_error);
        else{
            mysqli_set_charset($conn,"utf8");
            $stmt = $conn->prepare("INSERT INTO `user_google` (`FULL_NAME`,`LANG`, `EMAIL`, `SUB_GID`) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssss", $UserData["name"],$UserData["locale"],$UserData["email"],$UserData["sub"]);
            if($stmt->execute()){
                $UserData['newUser']= 'yes';
            }
            else{
                $UserData['newUser']= "no";
                $UserData['Error']= mysqli_error($conn);
            }$tsid=mysqli_insert_id($conn);
            $stmt->close();
        }$conn->close();

        } else {
        // Invalid ID token
        $UserData['loginAttempt']=false;
        $UserData['message']="Login Error!";
        }
        // echo json_encode($UserData);
    }
    else $UserData['message']="Vendor Not Found:Check GoogleAPIsClient folderin ".$flnk_tvar0;
    echo json_encode($UserData);
    // else echo "Vendor Not Found:Check GoogleAPIsClient folderin DIZ/AddOn/".$flnk_tvar0;
    //echo $flnk_tvar0;
}
?>
