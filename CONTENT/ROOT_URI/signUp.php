<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<style>
  .abcRioButton {
      width: 200px !important;
      margin: auto;
  }
</style>
<?php 
  $link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $link->set_charset("utf8");
   if(isset($_POST['fbId'])){
      $fbId = $_POST['fbId'];
      $fbName = $_POST['fbName'];
      $fbEmail = $_POST['fbEmail'];
      $fbImage = $_POST['image'];
      
      $sqlFb = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$fbId'";
      $resultFb = mysqli_query($link, $sqlFb);
      if(mysqli_num_rows($resultFb)>0){
        $_SESSION["LoggedIn"]=true;
        $_SESSION["user"] = $email;
        $_SESSION["userId"] = $row['UNI_ID'];
        $_SESSION["fbLoggedIn"] = true;
        $_SESSION["userName"] = $row['NAME'];
        echo "<script>
            window.location.href='/';
          </script>";
      }else{
        $stmt = $link->prepare("INSERT INTO ATHENEUM_STUDENT (`UNI_ID`, `NAME`, `EMAIL`, `STATUS`) VALUES (?, ?, ?,'VERIFIED')");
        $stmt->bind_param("sss", $fbId, $fbName, $fbEmail); 
        if ($stmt->execute()) {
          
          mkdir("CONTENT/UPLOADS/USERS/".$fbId."/",  0755, true);

          $ch = curl_init($fbImage);
          $fp = fopen("CONTENT/UPLOADS/USERS/".$fbId."/propic.jpg", 'wb');
          curl_setopt($ch, CURLOPT_FILE, $fp);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_exec($ch);
          curl_close($ch);
          fclose($fp);
          
          // Function to write image into file 
           
          $_SESSION["LoggedIn"]=true;
          $_SESSION["user"] = $email;
          $_SESSION["userId"] = $row['UNI_ID'];
          $_SESSION["fbLoggedIn"] = true;
          $_SESSION["userName"] = $row['NAME'];
          echo "<script>
              window.location.href='/';
            </script>";
          }else{
            echo "Something wrong ! please try again!";
          }
      }
    }
 ?>
<!-- FB LOGIN -->

 <!-- GOOGLE FORM SUBMIT -->

  <?php 
    if(isset($_POST['googleId'])){
      $googleId = $_POST['googleId'];
      $googleName = $_POST['googleName'];
      $googleEmail = $_POST['googleEmail'];
      $googleImage = $_POST['googleImage'];
      $sqlGoogle = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$googleEmail'";
      $resultGoogle = mysqli_query($link, $sqlGoogle);
      if(mysqli_num_rows($resultGoogle)>0){
        $row = mysqli_fetch_array($resultGoogle,MYSQLI_ASSOC);
        $_SESSION["LoggedIn"]=true;
        $_SESSION["user"] = $googleEmail;
        $_SESSION["userId"] = $row['UNI_ID'];
        $_SESSION["googleLoggedIn"] = true;
        $_SESSION["userName"] = $row['NAME'];
        echo "<script>
            window.location.href='/';
          </script>";
      }else{
        $stmt = $link->prepare("INSERT INTO ATHENEUM_STUDENT (`UNI_ID`, `NAME`, `EMAIL`, `STATUS`) VALUES (?, ?, ?,'VERIFIED')");
        $stmt->bind_param("sss", $googleId, $googleName, $googleEmail); 
        if ($stmt->execute()) {
          
          mkdir("CONTENT/UPLOADS/USERS/".$googleId."/",  0755, true);

          // $ch = curl_init($googleImage);
          // $fp = fopen("CONTENT/UPLOADS/USERS/".$googleImage."/propic.jpg", 'wb');
          // curl_setopt($ch, CURLOPT_FILE, $fp);
          // curl_setopt($ch, CURLOPT_HEADER, 0);
          // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          // curl_exec($ch);
          // curl_close($ch);
          // fclose($fp);

          $url = $googleImage;  
            
          $img = 'propic.jpg';  
            
          // Function to write image into file 
          file_put_contents("CONTENT/UPLOADS/USERS/".$googleId."/propic.jpg", file_get_contents($url)); 
            
          echo "File downloaded!";
          
          // Function to write image into file 
           
          $_SESSION["LoggedIn"]=true;
          $_SESSION["user"] = $googleEmail;
          $_SESSION["userId"] = $googleId;
          $_SESSION["googleLoggedIn"] = true;
          $_SESSION["userName"] = $googleName;
          echo "<script>
              window.location.href='/';
            </script>";
          }else{
            echo "Something wrong ! please try again!";
          }
      }
    }
  ?>

  <!-- GOOGLE FORM ENDS -->
<script>

  function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
    console.log('statusChangeCallback');
    console.log(response);                   // The current login status of the person.
    if (response.status === 'connected') {   // Logged into your webpage and Facebook.
      testAPI();  
    } else {  
      console.log("sorry");                               // Not logged into your webpage or we are unable to tell.
      // document.getElementById('status').innerHTML = 'Please log ' +
        // 'into this webpage.';
    }
  }


  function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function(response) {   // See the onlogin handler
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '3705294229488190',
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : 'v6.0'                    // Use this Graph API version for this call.
    });

    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
      statusChangeCallback(response);        // Returns the login status.
    });
  };

  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

 
  function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
    console.log('Welcome!  Fetching your information.... ');
    var uid = 0;
    FB.api(
  '/me',
  'GET',
  {"fields":"id,name,picture,email,locale"},
  function(response) {
      // Insert your code here
      console.log(response);
      uid = response.id;
      document.getElementById("fbId").value = uid;
      document.getElementById("fbName").value = response.name;
      document.getElementById("fbEmail").value = response.email;
      document.getElementById("fbImage").value = response.picture.data.url;
      document.getElementById("fbForm").submit();
    }
  );
  }

    function facebookLogout(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                FB.logout(function(response) {
                    // this part just clears the $_SESSION var
                    // replace with your own code
                    // $.post("/logout").done(function() {
                    //     $('#status').html('<p>Logged out.</p>');
                    // });
                    console.log("logged out");
                });
            }
        });
    }

</script>


<?php if ($_SESSION['LoggedIn']){
  echo '<script>
    window.location.href = "/"
  </script>';
 } ?>

<div class="content-wrapper">
  <section class="content">
    <br>
<div class="container">
<?php
  if ($GLOBALS['alert_info']!="") {
    echo $GLOBALS['alert_info'];
  }
  if (isset($_GET['refer'])) {
    $referalId = $_GET['refer'];
  }
  
?>
<meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="153383811061-o40ejrbftdaptc54jpp7cg5sq9pe2pf0.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
<div class="row">
<div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
<div class="">
  <div class="register-logo">
    <a href="https://atheneumglobal.education"><b>Atheneum Global College</b></a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form method="post" id="studentSignUp" enctype=“multipart/form-data”>
        <input type="hidden" name="s_Hash" value="<?php echo $_SESSION['s_Hash']; ?>">
        <input type="hidden" name="formName" value="studentSignUp">
        <input type="hidden" name="referalId" value="<?php echo $referalId; ?>">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Full name" name="name" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="number" class="form-control" placeholder="Phone Number" name="phone" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password1" class="form-control" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password2" class="form-control" placeholder="Retype password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="input-group mb-3">
              <select id="country" name ="country" class="form-control" id="sel1"></select>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-flag"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="input-group mb-3">
              <select name ="state" id ="state" class="form-control" id="sel1"></select>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-city"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree" required>
              <label for="agreeTerms">
               I agree to the terms
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <input type="submit" value="REGISTER" name="register" class="btn btn-primary btn-block">
          </div>
          <!-- /.col -->
        </div>
      </form>
       <form method="POST" id="fbForm">
        <input type="hidden" id="fbId" name="fbId">
        <input type="hidden" id="fbName" name="fbName">
        <input type="hidden" id="fbEmail" name="fbEmail">
        <input type="hidden" id="fbImage" name="image">
      </form>
       <form method="POST" id="googleForm">
        <input type="hidden" id="googleId" name="googleId">
        <input type="hidden" id="googleName" name="googleName">
        <input type="hidden" id="googleEmail" name="googleEmail">
        <input type="hidden" id="googleImage" name="googleImage">
      </form>

      <div class="social-auth-links text-center">
        <p>- OR -</p>
        <div class="fb-login-button btn btn-block" scope="public_profile,email" onlogin="checkLoginState();" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
        <div class="g-signin2 ml-auto mr-auto" data-onsuccess="onSignIn" data-theme="dark" style="margin-left:50px;"></div>
      </div>

      <a href="signIn" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
</div>
</div>


</div>

<script type="text/javascript" src="/JS/countries.js"></script>
<script language="javascript">
  populateCountries("country", "state"); 
</script>