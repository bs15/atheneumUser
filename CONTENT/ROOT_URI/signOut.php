


<?php 
 if(isset($_SESSION['LoggedIn'])){
 	// $_SESSION['logout'];
    
    // echo 
    if (isset($_SESSION['fbLoggedIn'])) {
            session_destroy();
      echo "<script>

      function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
        console.log('statusChangeCallback');
        console.log(response);                   // The current login status of the person.
        if (response.status === 'connected') {   // Logged into your webpage and Facebook.
          disconnect();  
        } else {  
          console.log('sorry');                               // Not logged into your webpage or we are unable to tell.
          // document.getElementById('status').innerHTML = 'Please log ' +
            // 'into this webpage.';
        }
      }


      function checkLoginState() {               // Called when a person is finished with the Login Button.
        FB.getLoginStatus(function(response) {   // See the onlogin handler
          statusChangeCallback(response);
        });
      }


      window.fbAsyncInit = function() {
        FB.init({
          appId      : '2683421201777035',
          cookie     : true,                     // Enable cookies to allow the server to access the session.
          xfbml      : true,                     // Parse social plugins on this webpage.
          version    : 'v6.0'                    // Use this Graph API version for this call.
        });

        FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
          statusChangeCallback(response);        // Returns the login status.
        });
      };

      
      (function(d, s, id) {                      // Load the SDK asynchronously
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

        function disconnect(){
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.logout(function(response) {
                        console.log('logged out');
                        window.location.href = '/';
                    });
                }
            });
        }

        

    </script>";
    	
    }else if(isset($_SESSION['googleLoggedIn'])) {
      session_destroy();
      echo '<div class="text-center">
              <h2>You have Successfully Signed out from the site.</h2>
              <a href="/" class="btn btn-success">Go to Home</a>
              <a href="https://accounts.google.com/logout" class="btn btn-danger">Sign out from Google</a>
            </div>';

    }
    else{
      session_destroy();
      echo "<script>
       window.location.href='/';
     </script>";

    }
    
	// header("Location: _home");

    
}


 ?>







