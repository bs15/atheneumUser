<style type="text/css">
  .headingC{
    
    background: #4b6cb7;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #182848, #4b6cb7);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #182848, #4b6cb7); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  }
</style>

<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0"></script>
<?php 
	$link = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $email = $_SESSION["user"];
  $userId = $_SESSION["userId"];
  $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$userId'";
  $result = mysqli_query($link,$sql);

 if($result){
      if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $userName = $row["NAME"];
        $email = $row["EMAIL"];
        $phone = $row["PHONE"];
        $programName = $row['PROGRAM_NAME'];
        $programType = $row['PROGRAM_TYPE'];
        $courseDetails = $row["COURSE_DETAILS"];
        $referalsDb = $row['REFERALS'];
        if ($referalsDb != null) {
          $referals = explode(",", $referalsDb);
          $totalRef = count($referals); 
        }else{
          $totalRef = 0;
        }
        $courseDetails = json_decode($courseDetails);
        if ($courseDetails != null) {
          $courseUrl = $courseDetails->{"url"};
          $courseUser = $courseDetails->{"user"};
          $coursePassword = $courseDetails->{"password"};
        }
        $earning = $row['EARNING'];
        $jsonDetails = $row["DETAILS"];
        $jsonDetails = json_decode($jsonDetails);
        if ($jsonDetails != null) {
          $country = $jsonDetails->{'country'};
          $state = $jsonDetails->{'state'};          
          if ($country == "India") {
           $in = true;
          }
        }
      }
  }
  if (isset($_POST['flag'])) {
    $image = $_FILES['propic']['name'];
    $imgName = "propic.jpg";
    if (!file_exists("CONTENT/UPLOADS/USERS/".$userId)) {
      mkdir("CONTENT/UPLOADS/USERS/".$userId,  0755, true);
    }
    
    $target = "CONTENT/UPLOADS/USERS/".$userId."/".$imgName;
    if (move_uploaded_file($_FILES['propic']['tmp_name'], $target)) {
      // $msg = "Image uploaded successfully";
      echo '<div class=container><div class="alert alert-success">Successfully updated profile picture</div></div>';
    }else{
      echo '<div class=container><div class="alert alert-danger">Error occured! Please try again!</div></div>';
      
    }
  }
 ?>

 <!-- ---------UPDATE PROFILE FORM BACKEND----------- -->

 <?php 
    if (isset($_POST['profileSubmit'])) {
      $editname = $_POST['name'];
      $editphone = $_POST['phone'];
      $editcountry = $_POST['country'];
      $editstate = $_POST['state'];
      $editdetails = array("country" => $editcountry, "state" => $editstate);
      $editdetails =  json_encode($editdetails);
      $sqlUpdate = "UPDATE ATHENEUM_STUDENT SET NAME = '$editname', PHONE = '$editphone', DETAILS = '$editdetails' WHERE UNI_ID = '$userId'";
      $resultUpdate = mysqli_query($link, $sqlUpdate);
      if($resultUpdate){
        echo '<script>location.reload();</script>';
         echo '<div class=container><div class="alert alert-success">Successfully updated profile picture</div></div>';
       }else{
          echo '<div class=container><div class="alert alert-danger">Error occured! Please try again!</div></div>';
       }
    }

  ?>

 <?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }


 ?>

<?php if($_SESSION['LoggedIn']): ?>
<div class="content-wrapper">
  <section class="content">
    <br>
<div class="container">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
            <div class="row">
              <div class="col-lg-6 col-sm-12 ml-auto mr-auto">
                 <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                      <div class="user text-center">
                        <?php 
                          if (file_exists("CONTENT/UPLOADS/USERS/".$userId."/propic.jpg")) { 
                           echo '<img src="CONTENT/UPLOADS/USERS/'.$userId.'/propic.jpg" style="border-radius: 50%; border: 1px solid" height="80px" width="80px;">';
                         }else{ ?>
                            <img src="https://lh3.googleusercontent.com/proxy/-E_xTfSYkd31TEwVQ1KYQsM5guSl3nytWQnn7801uYkPvC--lbF8hdnDsmptvmn88SdkNgf6oIoVa1bvq0gLcGy7" style="border-radius: 50%; border: 1px solid" height="80px" width="80px;">
                         <?php } ?>
                        <p class="lead text-muted"><?php echo $userName; ?></p>
                        <a href="#" class="btn btn-sm btn-success" id="upload_pro"><i class="fas fa-upload"></i>&nbsp;Profile Picture</a>
                        <a href="changePassword" class="btn btn-sm btn-danger"><i class="fas fa-lock"></i>&nbsp;Change Password</a>
                        <a href="/" class="btn btn-sm btn-warning"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a>
                            <!-- <a href="changePassword" class="btn btn-warning"><i class="fas fa-user"></i>&nbsp;Update Details</a> -->
                      </div><br>
                      <div class="details">
                        <?php 
                        $sql = "SELECT * FROM ATHENEUM_STUDENT";
                        $result = mysqli_query($link,$sql);
                        $registeredUser = array();
                        $enrolledUser = array();
                        $registeredCount = 0;
                        $enrolledCount = 0;
                        if ($referalsDb != null) {
                          if(mysqli_num_rows($result)>0){
                              while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $userEmail = $row['EMAIL'];
                                $paid = $row['PAID'];
                                if($userEmail != $email){
                                  if (in_array($userEmail, $referals)){
                                    array_push($registeredUser, $userEmail);
                                    if ($paid != 0) {
                                    array_push($enrolledUser, $userEmail);
                                    }
                                  }
                                  
                                }
                                
                              }
                            }
                            $registeredCount = count($registeredUser);
                            $enrolledCount = count($enrolledUser);
                        }

                         ?>
                        <h5>User details</h5> <hr>
                        <div class="row">
                          <div class="col">Enrolled Courses: </div>
                          <div class="col"><span class="float-right"><?php if($programName != null) echo "1"; else echo "0"; ?></span></div>
                        </div>
                        <div class="row">
                          <div class="col">Total referals: </div>
                          <div class="col"><span class="float-right"><?php echo $totalRef; ?></span></div>
                        </div>
                        <div class="row">
                          <div class="col">Registered Referals: </div>
                          <div class="col"><span class="float-right"><?php echo $registeredCount; ?></span></div>
                        </div>
                        <div class="row">
                          <div class="col">Enrolled Referals: </div>
                          <div class="col"><span class="float-right"><?php echo $enrolledCount; ?></span></div>
                        </div>
                        <div class="row">
                          <div class="col">Earned Money: </div>
                          <div class="col"><span class="float-right"><?php echo $earning; ?></span></div>
                        </div>
                        <hr>
                        <div class="col-8 ml-auto mr-auto">
                          <form method="POST"> 
                          
                        
                           <div class="row">
                            <div class="col-10">
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="editName" readonly required value="<?php echo $userName; ?>">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-2">
                              <button type="button" class="btn btn-success" title="Edit Details" id="editNameBtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-12">
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Name" name="email" readonly required value="<?php echo $email; ?>">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10">
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Phone Number" id="editPhone" name="phone" readonly required value="<?php echo $phone; ?>">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-phone"></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-2">
                              <button type="button" class="btn btn-success" title="Edit Details" id="editPhoneBtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10">
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Country (Eg:- India)" name="country" id="editCountry" readonly required value="<?php echo $country; ?>">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-flag"></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-2">
                              <button type="button" class="btn btn-success" title="Edit Details"  id="editCountryBtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10">
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="State" name="state" id="editState" readonly required value="<?php echo $state; ?>">
                                <div class="input-group-append">
                                  <div class="input-group-text">
                                    <span class="fas fa-city"></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-2">
                              <button type="button" class="btn btn-success" title="Edit Details" id="editStateBtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <input class="btn btn-success" type="submit" value="Update Profile" name="profileSubmit">
                        </div>    
                       </div>
                     </form>
                    </div> 
                    <!-- /.card-body -->
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div><br>
      </div>
</div>
<form id="inputForm9" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="size" value="1000000">
    <input type="hidden" name="flag" value="1000000">
    <input id="upload1" type="file" style="display: none; color: inherit;" name="propic"
     onchange="form.submit()" />
 </form>
<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="register-logo">
        <h2><b>Atheneum Global Teacher Training College</b></h2>
        <p>User Dashboard</p>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="ml-auto mr-auto text-center">
            <img src="/IMAGES/logo.jpeg" width="50%" height="50%">
          </div>
          <div class="card-text">Welcome to Atheneum Global Teacher Training College College User Dashboard. 
            You can win exciting rewards and keep adding cashback into your wallet.  
            <a href="signUp">Register</a> and <a href="signIn">login</a> now to learn more...</div>
         
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </div>

<?php endif; ?>
<script type="text/javascript">
  $(function () {
        $("#editNameBtn").click(function () {
          // console.log("changes");
          $("#editName").removeAttr("readonly");
        });
        $("#editPhoneBtn").click(function () {
          // console.log("changes");
          $("#editPhone").removeAttr("readonly");
        });
        $("#editCountryBtn").click(function () {
          // console.log("changes");
          $("#editCountry").removeAttr("readonly");
        });
        $("#editStateBtn").click(function () {
          // console.log("changes");
          $("#editState").removeAttr("readonly");
        });
    });
   $(function(){
      $("#upload_pro").on('click', function(e){
          e.preventDefault();
          $("#upload1:hidden").trigger('click');
      });
    });
</script>
