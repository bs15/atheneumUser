<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
<style>
        .abcRioButton {
            width: 200px !important;
            margin: auto;
        }
    </style>
<?php 
  $link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $link->set_charset("utf8");

 ?>

<?php if ($_SESSION['LoggedIn']){
  echo '<script>
    window.location.href = "/"
  </script>';
 } 
 ?>

 <!-- FACEBOOK DATA SUBMIT -->

 <?php 
    if(isset($_POST['fbId'])){
      $fbId = $_POST['fbId'];
      $fbName = $_POST['fbName'];
      $fbEmail = $_POST['fbEmail'];
      $fbImage = $_POST['image'];
      $sqlFb = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$fbEmail'";
      $resultFb = mysqli_query($link, $sqlFb);
      if ($resultFb) {
        if(mysqli_num_rows($resultFb)>0){
          $row = mysqli_fetch_array($resultFb,MYSQLI_ASSOC);
          $_SESSION["LoggedIn"]=true;
          $_SESSION["user"] = $fbEmail;
          $_SESSION["userId"] = $row['UNI_ID'];
          $_SESSION["fbLoggedIn"] = true;
          $_SESSION["userName"] = $row['NAME'];
          echo "<script>
              window.location.href='/';
            </script>";
        }else{
          $stmt = $link->prepare("INSERT INTO ATHENEUM_STUDENT (`UNI_ID`, `NAME`, `EMAIL`, `STATUS`) VALUES (?, ?, ?,'VERIFIED')");
          if ($stmt) {
            $stmt->bind_param("sss", $fbId, $fbName, $fbEmail); 
            if ($stmt->execute()) {
            
            mkdir("CONTENT/UPLOADS/USERS/".$fbId."/",  0755, true);

            $ch = curl_init($fbImage);
            $fp = fopen("CONTENT/UPLOADS/USERS/".$fbId."/propic.jpg", 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            
            // Function to write image into file 
             
            $_SESSION["LoggedIn"]=true;
            $_SESSION["user"] = $fbEmail;
            $_SESSION["userId"] = $row['UNI_ID'];
            $_SESSION["fbLoggedIn"] = true;
            $_SESSION["userName"] = $row['NAME'];
            echo "<script>
                window.location.href='/';
              </script>";
            }else{
              echo "Something wrong ! please try again!";
            }
          }
        } 
      }
    }
  ?>

  <!-- GOOGLE FORM SUBMIT -->

  <?php 
    if(isset($_POST['googleId'])){
      $googleId = $_POST['googleId'];
      $googleName = $_POST['googleName'];
      $googleEmail = $_POST['googleEmail'];
      $googleImage = $_POST['googleImage'];
      $sqlGoogle = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$googleEmail'";
      $resultGoogle = mysqli_query($link, $sqlGoogle);
      if(mysqli_num_rows($resultGoogle)>0){
        $row = mysqli_fetch_array($resultGoogle,MYSQLI_ASSOC);
        $_SESSION["LoggedIn"]=true;
        $_SESSION["user"] = $googleEmail;
        $_SESSION["userId"] = $row['UNI_ID'];
        $_SESSION["googleLoggedIn"] = true;
        $_SESSION["userName"] = $row['NAME'];
        echo "<script>
            window.location.href='/';
          </script>";
      }else{
        $stmt = $link->prepare("INSERT INTO ATHENEUM_STUDENT (`UNI_ID`, `NAME`, `EMAIL`, `STATUS`) VALUES (?, ?, ?,'VERIFIED')");
        $stmt->bind_param("sss", $googleId, $googleName, $googleEmail); 
        if ($stmt->execute()) {
          
          mkdir("CONTENT/UPLOADS/USERS/".$googleId."/",  0755, true);

          // $ch = curl_init($googleImage);
          // $fp = fopen("CONTENT/UPLOADS/USERS/".$googleImage."/propic.jpg", 'wb');
          // curl_setopt($ch, CURLOPT_FILE, $fp);
          // curl_setopt($ch, CURLOPT_HEADER, 0);
          // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          // curl_exec($ch);
          // curl_close($ch);
          // fclose($fp);

          $url = $googleImage;  
            
          $img = 'propic.jpg';  
            
          // Function to write image into file 
          file_put_contents("CONTENT/UPLOADS/USERS/".$googleId."/propic.jpg", file_get_contents($url)); 
            
          echo "File downloaded!";
          
          // Function to write image into file 
           
          $_SESSION["LoggedIn"]=true;
          $_SESSION["user"] = $googleEmail;
          $_SESSION["userId"] = $googleId;
          $_SESSION["googleLoggedIn"] = true;
          $_SESSION["userName"] = $googleName;
          echo "<script>
              window.location.href='/';
            </script>";
          }else{
            echo "Something wrong ! please try again!";
          }
      }
    }
  ?>

  <!-- GOOGLE FORM ENDS -->

<div class="content-wrapper">
  <section class="content">
    <br>
<div class="container">
<?php
  if ($GLOBALS['alert_info']!="") {
    echo $GLOBALS['alert_info'];
  }
?>

<!----------------- FACEBOOK LOGIN -------------------->
<script>

  function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
    console.log('statusChangeCallback');
    console.log(response);                   // The current login status of the person.
    if (response.status === 'connected') {   // Logged into your webpage and Facebook.
      testAPI();  
    } else {  
      console.log("sorry");                               // Not logged into your webpage or we are unable to tell.
      // document.getElementById('status').innerHTML = 'Please log ' +
        // 'into this webpage.';
    }
  }


  function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function(response) {   // See the onlogin handler
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2683421201777035',
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : 'v6.0'                    // Use this Graph API version for this call.
    });

    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
      statusChangeCallback(response);        // Returns the login status.
    });
  };

  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

 
  function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
    console.log('Welcome!  Fetching your information.... ');
    var uid = 0;
    FB.api(
  '/me',
  'GET',
  {"locale": "en_US","fields":"id,name,picture,email,locale"},
  function(response) {
      // Insert your code here
      console.log(response);
      uid = response.id;
      document.getElementById("fbId").value = uid;
      document.getElementById("fbName").value = response.name;
      document.getElementById("fbEmail").value = response.email;
      document.getElementById("fbImage").value = response.picture.data.url;
      console.log(document.getElementById("fbImage").value);
      document.getElementById("fbForm").submit();
    }
  );
  }

    function facebookLogout(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                FB.logout(function(response) {
                    console.log("logged out");
                });
            }
        });
    }

</script>

<!----------------- FACEBOOK LOGIN ENDS -------------------->

<!------------------- GOOGLE LOGIN ---------------------->


    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="153383811061-o40ejrbftdaptc54jpp7cg5sq9pe2pf0.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <title>Login Using Google</title>
    <script>
        function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var id_token = googleUser.getAuthResponse().id_token;
        // console.log('idToken: ' + id_token);
        console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
        document.getElementById("googleId").value = profile.getId();
        document.getElementById("googleName").value = profile.getName();
        document.getElementById("googleEmail").value = profile.getEmail();
        document.getElementById("googleImage").value = profile.getImageUrl();
        // console.log(document.getElementById("fbImage").value);
        document.getElementById("googleForm").submit();
        }
        // function googleSignOut(){
        //   var auth2 = gapi.auth2.getAuthInstance();
        //   auth2.signOut().then(function () {
        //   console.log('User signed out.');
        //   });
        // }
    </script>
    <!-- GOOGLE SIGN IN ENDS -->
<div class="row">
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0&appId=3705294229488190&autoLogAppEvents=1"></script>
<div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
<div class="">
  <div class="register-logo">
    <a href="https://atheneumglobal.education"><b>Atheneum Global College</b></a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="s_Hash" value="<?php echo $_SESSION['s_Hash']; ?>">
        <input type="hidden" name="formName" value="studentSignIn">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" name="email" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <form method="POST" id="fbForm">
        <input type="hidden" id="fbId" name="fbId">
        <input type="hidden" id="fbName" name="fbName">
        <input type="hidden" id="fbEmail" name="fbEmail">
        <input type="hidden" id="fbImage" name="image">
      </form>
      <form method="POST" id="googleForm">
        <input type="hidden" id="googleId" name="googleId">
        <input type="hidden" id="googleName" name="googleName">
        <input type="hidden" id="googleEmail" name="googleEmail">
        <input type="hidden" id="googleImage" name="googleImage">
      </form>

      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <div class="g-signin2 ml-auto mr-auto" data-onsuccess="onSignIn" data-theme="dark" style="margin-left:50px;"></div>
        <div class="fb-login-button btn btn-block" scope="public_profile,email" onlogin="checkLoginState();" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
        <!-- <a href="#" class="btn btn-block btn-danger" onclick="onSignIn()">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a> -->
        
      </div>
      <!-- /.social-auth-links -->

      <p class="mb-1">
        <a href="forgotpassword">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="signUp" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- <button onclick="googleSignOut()">Signout</button> -->
<!-- /.register-box -->

<!-- jQuery -->
</div>
</div>

</div>