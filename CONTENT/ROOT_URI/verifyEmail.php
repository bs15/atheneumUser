<script>
  if ( window.history.replaceState ) {
      window.history.replaceState( null, null, window.location.href );
  }
</script>
<?php 
 $link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $link->set_charset("utf8");
   function httpPost($url,$params)
  {
    $postData = '';
     //create name value pairs seperated by &
   foreach($params as $k => $v) 
   { 
      $postData .= $k . '='.$v.'&'; 
   }
   $postData = rtrim($postData, '&');
 
    $ch = curl_init();  
 
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
 
    $output=curl_exec($ch);
 
    curl_close($ch);
    return $output;
   
  }

 ?>

<div class="content-wrapper">
  <section class="content">
    <br>

<div class="container">
<div class="row">
<div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
<div class="">
  <div class="register-logo">
    <a href="https://atheneumglobal.education"><b>Atheneum Global College</b></a>
  </div>

  <div class="card">
    <div class="card-body login-card-body">
      <?php 
        $userId = $_GET['user'];
        $verification = $_GET['verification'];
        $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$userId' AND VERIFICATION_CODE = '$verification'";
        $result = mysqli_query($link,$sql);
        if(mysqli_num_rows($result)>0){ 
          $sql = "UPDATE ATHENEUM_STUDENT SET STATUS = 'VERIFIED' WHERE UNI_ID = '$userId'";
          $result = mysqli_query($link,$sql);
          if ($result) {
            $_SESSION["LoggedIn"]=true;
            $_SESSION["user"] = $email;
            $_SESSION["userId"] = $row['UNI_ID'];
            $_SESSION["userName"] = $row['NAME'];
            echo '<p class="login-box-msg">Congratulation! Your Mail is verified.</p>';
            echo '<p class="login-box-msg">Please <a href="/">Go to your dashboard</a></p>';
            $directors = array('info@atheneumglobal.com','director@beanstalkedu.com');
            $reciever = implode(",", $directors);
            $params = array(
             "sendMail" => true,
             "reciever" => $reciever,
             "sender" => "Atheneum Global",
             "senderMail" => "noreply@atheneumglobal.com",
             "subject" => "New Registration",
             "message" => "<html>
              <head>
                <title>Welcome student</title>
              </head>
              <body>
              <h3>A new student has successfully registered in Atheneum Global User dashboard. </h3>
              <p><a href='https://admin.atheneumglobal.education/'>Click here to see details</a></p>
              </body>
              </html>"
            );
            $response = httpPost("https://api.teenybeans.in/API/mailApi/v1/",$params);
          }
        }else{
          echo '<p class="login-box-msg">Your Mail is not verified.</p>';
          echo '<p class="login-box-msg">Please try again!</p>';
        }
       ?>
      

      
    </div>
    <!-- /.login-card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
</div>
</div>

</div>